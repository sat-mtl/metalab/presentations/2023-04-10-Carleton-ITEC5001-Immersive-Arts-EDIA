---
title: Challenges and Opportunities for Equity, Diversity, Inclusion, and Accessibility in Immersive Arts
subtitle: CSIT ITEC 5001 Graduate Student Seminar
author: Christian Frisson
institute: Society for Arts and Technology [SAT]
separator: <!--s-->
verticalSeparator: <!--v-->
theme: white 
revealOptions:
  transition: 'none' 
  loop: false 
  slideNumber: "c/t"
  autoPlayMedia: true
--- 

<!-- .slide: id="start" -->
  <h2 data-i18n="title">Challenges and Opportunities <br/>for Equity, Diversity, Inclusion, and Accessibility <br/>in Immersive Arts</h2>
  <h3 data-i18n="conference"><a href="https://www.csit.carleton.ca/">CSIT</a> ITEC 5001 <br/>Graduate Student Seminar</h3>
  <h4 data-i18n="subtitle">2023-04-10</h4>

  <div class="authors">
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name">
      <a href="mailto:christian@frisson.re" title="Christian Frisson's Email">Christian</a></span> <a href="https://frisson.re" title="Christian Frisson's Website">Frisson</a></span>
      </div>
    </div>
  </div>


<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/1-LOGO_SAT_noir_sans_texte.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

[[SAT](https://sat.qc.ca/)] [metalab](https://sat-mtl.gitlab.io/metalab/)

<!--s-->
  <!-- .slide: id="sat" -->
  <h2 data-i18n="title"><a href="https://sat.qc.ca">Society for Arts and Technology</a></h2>

<div class="row">
<div class="col-20 fragment">
  <img src="images/sat.svg" style="height:700px;margin-top:-50px" />  
</div>
<div class="col-80 fragment">

<iframe src="https://player.vimeo.com/video/390637480?loop=false&amp;autoplay=false&amp;muted=false&amp;gesture=media&amp;playsinline=true&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=false&amp;customControls=true" allowfullscreen="" allow="autoplay; fullscreen; picture-in-picture; encrypted-media; accelerometer; gyroscope" width="640" height="360" title="Player for Société des arts technologiques (SAT)" data-ready="true" tabindex="-1"></iframe>

</div>
</div>

Notes:

> In the heart of Montreal, the SAT is a unique place that offers the public immersive experiences in its famous dome, but also concerts, workshops, conferences, exhibitions… Hundreds of events are presented there every year.

> Founded in 1996, the Society for Arts and Technology [SAT] is a non-profit organization dedicated to digital culture. With its triple mission as a center for the arts, training and research, the SAT is a gathering space for diverse talent, curiosity, and knowledge. It is recognized internationally for its active, leading role in developing technologies for immersive creation, mixed realities and telepresence. The Society for Arts and Technology is a place of collective learning that holds the promise of exploring technology to infuse it with more meaning, magic and humanity. 

<!--v-->
  <!-- .slide: id="livepose-experimentations" -->
  <h2 data-i18n="title">Immersive Arts</h2>
  <iframe src="https://player.vimeo.com/video/639532760"  data-audio-controls  width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--v-->

# Directions for Innovation

<div class="row">

<div class="col-50">

Left direction

<div class="figure">
<img class="thumb" alt="Facebook CEO Mark Zuckerberg (left) walks past a crowd with virtual reality (VR) headsets" src="images/3rdparty/zuckerberg-vr-crowd.jpg"/>
<!-- <div class="cite"> -->

https://www.businessinsider.com/metaverse-facebook-hiring-10000-people-to-build-mark-zuckerbergs-project-2021-10

<!-- </div> -->
</div>
</div>

<div class="col-50 fragment">

Right direction

<div class="figure">
<img class="thumb" alt="" src="images/3rdparty/BretVictor/SeeingSpaces.jpg"/>
<!-- <div class="cite"> -->

http://worrydream.com/#!/SeeingSpaces

<!-- </div> -->
</div>

</div>
</div>

Notes:
* Equip/Instrument spaces rather than people
* Do not rely on hardware brought by people for them to live the experience
* A tool for inclusivity

<!--s-->
  <!-- .slide: id="sat-tools" -->
  <h1>Ecosystem of tools at the SAT</h1>
  <h2>Inter-operability and modalities</h2>
  <div class="fig-container" data-file="images/sat-tools/default.html" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->
  <!-- .slide: id="sat-tools-redux" -->
  <h1>Ecosystem of tools at the SAT</h1>
  <h2>Inter-operability and modalities</h2>
  <h3>Redux</h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->
  <!-- .slide: id="sat-tools-redux-livepose" -->
  <h1>Computer vision</h1>
  <h2>Camera-based group interaction with LivePose</h2>
  <h3>Towards training pose and action for diversity, inclusion, and accessibility</h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html?t=LivePose" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->
  <!-- .slide: id="sat-tools-redux-splash" -->
  <h1>Human vision</h1>
  <h2>Projection mapping and colorimetry calibration with Splash</h2>
  <h3>Towards compensating for vision impairments</h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html?t=Splash" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->
  <!-- .slide: id="sat-tools-redux-haptic-floor" -->
  <h1>Haptic (tactile and kinesthetic) and audio</h1>
  <h2>Authoring for the Haptic Floor <br/>with proxies and by understanding explorable explanations</h2>
  <h3>Towards inclusive design with Blind people</h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html?t=haptic-floor" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->
  <!-- .slide: id="sat-tools-redux-satie" -->
  <h1>From audition to touch, <br/>towards olfaction and gustation</h1>
  <h2>Spatializing (ultra)sound haptics with SATIE</h2>
  <h3>Towards individualized immersions</h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html?t=SATIE" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->
  <h1>Outline</h1>
  <h2>For each of these 4 modalities</h2>
  <h3>Challenges</h3>
  <h3>Opportunities</h3>
  <h3>Take-home message</h3>

<!--s-->
  <!-- .slide: id="section-livepose" -->
  <h1>Computer vision</h1>
  <h2>Camera-based group interaction with LivePose</h2>
  <h3>Towards training pose and action for diversity, inclusion, and accessibility<h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html?t=LivePose" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->


<!--v-->

# Challenges

<!--v-->

# Challenges
## Democratizing camera-based group interaction


<div class="row">
<div class="col-50">

![](images/projects/MusicmotionHacklab2021/_mg_0123_sebastienroy.jpg)

Team: Percepto

</div>
<div class="col-50">

![](images/projects/MusicmotionHacklab2021/_mg_0382_sebastienroy.jpg)

Team: Le chant du canevas

</div>

MusicMotion Hacklab 2021 with/at the SAT, Digital Arts and Inclusion

Photos: Sébastien Roy

https://sat.qc.ca/fr/nouvelles/hacklab2021

</div>

Notes:
* Allow for everybody to be part of the experience
* Enable detection over large spaces
* A tool for bringing interactivity into the physical space
* Not a tool for surveillance!
* Challenge of being able to estimate poses and action for anybody, regardless of their specificity

<!--v-->
# Challenges
## Communicating using only body language

![images/projects/Satellite/the_mutual_penetration_of_physical_and_virtual_bodies.png](images/projects/Satellite/the_mutual_penetration_of_physical_and_virtual_bodies.png)

Ἐphemera One (Daria Smakhtina – Vadim Smakhtina) (Ru). 
The Mutual Penetration Of Physical And Virtual Bodies. 
Satellite Campus SAT VR/AR. 
Real/Virtual Experience in Satellite / Mozilla Hubs

https://sat.qc.ca/en/satellite-virtual-space - https://satellite.sat.qc.ca/ktWzSMr/

Notes:
> The installation is an artistic investigation of emerging forms of virtual and physical communications developed through Mozilla’s custom Hubs platforms. It is an interactive environment intended for two users to communicate using only with body language. The first user uses a virtual reality headset and is represented by an avatar. The avatar is located in a space, which is entirely controlled by the second user. By employing their body language, the second user changes the appearance of the room, which thus serves as a device for spatial communication, replacing spoken language with a language of non-orthogonal shapes.
* Avatar: representation of a white male

<!--v-->
# Opportunities

<!--v-->

# Opportunities
## Training systems for accessibility, diversity, inclusion

<div class="row">
<div class="col-50 fragment">

<iframe width="560" height="315" src="https://www.youtube.com/embed/-GVW2C2MXas" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

https://creo.ca/en/megaceta-2/

</div>
<div class="col-50 fragment">

![](images/projects/CREO/2022-CREO-Megaceta_i3d_scratch_matrix_image-2023-01-20.png)

https://github.com/open-mmlab/mmaction2

</div>
</div>

<!--v-->
  <h1>Opportunities</h1>
  <h2>Mozilla Responsible AI Challenge</h2>
  <div class="fig-container" data-file="https://web.archive.org/web/20230405155805/https://future.mozilla.org/builders-challenge/" style="height: auto;" data-scrollable="yes"></div>
  <div class="cite"><a href="https://future.mozilla.org/builders-challenge/">https://future.mozilla.org/builders-challenge/</a></div>


<!--v-->
 <h1>Take-home message</h1>
  <h2>Compete against yourself in student innovation challenges</h2>
  <div class="row">
  <div class="col-50">
  <div class="fig-container" data-file="https://web.archive.org/web/20221206211406/https://uist.acm.org/uist2013/contest.php" style="height: auto;" data-scrollable="yes"></div>
  <br/>
  <div class="cite"><a href="https://uist.acm.org/uist2013/contest.php">https://uist.acm.org/uist2013/contest.php</a></div>
    </div>
  <div class="col-50">
  <div class="fig-container" data-file="https://2021.worldhaptics.org/sic-program/" style="height: auto;" data-scrollable="yes"></div>
  <div class="cite"><a href="https://2021.worldhaptics.org/sic-program/">https://2021.worldhaptics.org/sic-program/</a></div>
    </div>
    </div>

<!--s-->
  <!-- .slide: id="section-splash" -->
  <h1>Human vision</h1>
  <h2>Projection mapping and colorimetry calibration with Splash</h2>
  <h3>Towards compensating for vision impairments<h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html?t=Splash" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->
  <!-- .slide: id="splash" -->
  <h2 data-i18n="title"><a href="https://gitlab.com/sat-mtl/tools/splash/splash">Splash</a></h2>
  <h3 data-i18n="title">Tool for projection mapping</h3>
  <iframe src="https://player.vimeo.com/video/314519230?muted=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture;" allowfullscreen></iframe>

<!--v-->

# Challenges

<h2 data-i18n="title">Calibrating color between projectors and humans</h2>

* banding
* potential precision loss due to colour spaces switching
* differences in projectors’ gamut and overlapping areas
* distortion due to geometric calibration
* colour bleeding

<video autoplay controls src="images/splash/colo_synced_trimmed.mp4" data-audio-controls width="100%" height="auto"></video>

<div class="cite">Eva Décorps, <b>Christian Frisson</b>, Emmanuel Durand. <a href="https://doi.org/10.1145/3562939.3565684">Colorimetry Evaluation for Video Mapping Rendering</a>, 28th ACM Symposium on Virtual Reality Software and Technology (VRST 2022)</div>

<!--v-->
  <!-- .slide: id="splash-colorimetry-intro" -->
  <h1 data-i18n="title">Opportunities</h1>
  <h2 data-i18n="title">Image quality assessment methods</h2>

  <h4>objective</h4>

  * mean squared error (MSE) 
  * peak signal-to-noise ratio (PSNR)

&#8594; physical meaning, fast to compute
  
  <h4>subjective</h4>

  * structural similarity (SSIM)
  * gradient magnitude similarity deviation (GMSD)

&#8594; better correlation with human perception

<div class="cite">Eva Décorps, <b>Christian Frisson</b>, Emmanuel Durand. <a href="https://doi.org/10.1145/3562939.3565684">Colorimetry Evaluation for Video Mapping Rendering</a>, 28th ACM Symposium on Virtual Reality Software and Technology (VRST 2022)</div>

<!--v-->
  <!-- .slide: id="splash-colorimetry-evaluation" -->
  <h1 data-i18n="title">Opportunities</h1>
  <h2 data-i18n="title">Evaluation</h2>

[Internal (only software)](#evaluation-internal)

[Overall (with hardware)](#evaluation-overall)

<div class="cite">Eva Décorps, <b>Christian Frisson</b>, Emmanuel Durand. <a href="https://doi.org/10.1145/3562939.3565684">Colorimetry Evaluation for Video Mapping Rendering</a>, 28th ACM Symposium on Virtual Reality Software and Technology (VRST 2022)</div>

<!--v-->
  <!-- .slide: id="splash-colorimetry-evaluation-internal" -->
  <h1 data-i18n="title">Opportunities</h1>
  <h2 data-i18n="title"><a href="#evaluation">Evaluation</a></h2>
  <h3 data-i18n="title">Internal (only software)</h3>
  <img src="images/splash/internal_evaluation_same_scale.png" style="height:350px" />  
  <br/>
  <img src="images/splash/internal_evaluation_table.png" style="height:100px" />  
  <div class="cite">Eva Décorps, <b>Christian Frisson</b>, Emmanuel Durand. <a href="https://doi.org/10.1145/3562939.3565684">Colorimetry Evaluation for Video Mapping Rendering</a>, 28th ACM Symposium on Virtual Reality Software and Technology (VRST 2022)</div>

<!--v-->
  <!-- .slide: id="splash-colorimetry-evaluation-overall" -->
  <h1 data-i18n="title">Opportunities</h1>
  <h2 data-i18n="title"><a href="#evaluation">Evaluation</a></h2>
  <h3 data-i18n="title">Overall (with hardware)</h3>
  <img src="images/splash/overall_evaluation_same_scale.png" style="height:350px" />  
  <br/>
  <img src="images/splash/overall_evaluation_table.png" style="height:100px" />
  <div class="cite">Eva Décorps, <b>Christian Frisson</b>, Emmanuel Durand. <a href="https://doi.org/10.1145/3562939.3565684">Colorimetry Evaluation for Video Mapping Rendering</a>, 28th ACM Symposium on Virtual Reality Software and Technology (VRST 2022)</div>

<!--v-->
  <!-- .slide: id="splash-colorimetry-future-work" -->
  <h1 data-i18n="title">Opportunities</h1>
  <h2 data-i18n="title">Future work</h2>
  
* testing more recent metrics such as LPIPS
* evaluating robustness to environmental lighting conditions

<div class="cite">Eva Décorps, <b>Christian Frisson</b>, Emmanuel Durand. <a href="https://doi.org/10.1145/3562939.3565684">Colorimetry Evaluation for Video Mapping Rendering</a>, 28th ACM Symposium on Virtual Reality Software and Technology (VRST 2022)</div>

<!--v-->
  <h1>Challenge</h1>
  <h2>Addressing color blindness in data visualization?</h2>
  <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Data visualization cares disproportionately far too much about designing for colorblindness relative to other disabilities that are more common (visual impairments included).<br><br>(A thread on disability, race, and patriarchy in data visualization.)</p>&mdash; Frank ⌁ (@FrankElavsky) <a href="https://twitter.com/FrankElavsky/status/1351311898428362754?ref_src=twsrc%5Etfw">January 18, 2021</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
  <div class="cite"><a href="https://twitter.com/FrankElavsky/status/1351311898428362754">https://twitter.com/FrankElavsky/status/1351311898428362754</a> | <a href="https://www.frank.computer/">https://www.frank.computer</a></div>

<!--v-->
  <h1>Challenge</h1>
  <h2>Addressing color blindness in data visualization?</h2>
  <blockquote class="twitter-tweet"><p lang="en" dir="ltr">~4.5% of people with northern European ancestry are colorblind. But less than half of a percent of women are.<br><br>This means that nearly 8% of men from a northern European background have some form of colorblindness.<br><br>*Colorblindness affects WHITE MEN the most.*</p>&mdash; Frank ⌁ (@FrankElavsky) <a href="https://twitter.com/FrankElavsky/status/1351311899435024384?ref_src=twsrc%5Etfw">January 18, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
  <div class="cite"><a href="https://twitter.com/FrankElavsky/status/1351311898428362754">https://twitter.com/FrankElavsky/status/1351311898428362754</a> | <a href="https://www.frank.computer/">https://www.frank.computer</a></div>

<!--v-->
  <h1>Challenge</h1>
  <h2>Addressing color blindness in data visualization?</h2>
  <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Why does this matter?<br><br>Because designers, scientists, and engineers in our field continue to produce palettes, guides, research, and tools for dealing with colorblindness when visualizing data.<br><br>But where are tools and resources for all the other kinds of disabilities out there?!</p>&mdash; Frank ⌁ (@FrankElavsky) <a href="https://twitter.com/FrankElavsky/status/1351311900538073091?ref_src=twsrc%5Etfw">January 18, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
  <div class="cite"><a href="https://twitter.com/FrankElavsky/status/1351311898428362754">https://twitter.com/FrankElavsky/status/1351311898428362754</a> | <a href="https://www.frank.computer/">https://www.frank.computer</a></div>

<!--v-->
  <h1>Opportunities</h1>
  <h2>Dagstuhl Seminar 23252: Inclusive Data Visualization</h2>
  <div class="fig-container" data-file="https://www.dagstuhl.de/en/seminars/seminar-calendar/seminar-details/23252" style="height: auto;" data-scrollable="yes"></div>
  <div class="cite"><a href="https://www.dagstuhl.de/23252">https://www.dagstuhl.de/23252</a></div>

<!--v-->
  <h1>Opportunities</h1>
  <h2>Dagstuhl Seminar 18441: Data Physicalization</h2>
  <div class="fig-container" data-file="https://www.dagstuhl.de/en/seminars/seminar-calendar/seminar-details/18441" style="height: auto;" data-scrollable="yes"></div>
  <div class="cite"><a href="https://www.dagstuhl.de/18441">https://www.dagstuhl.de/18441</a> | <a href="https://makingwithdata.org">https://makingwithdata.org</a></div>

<!--v-->
  <h1>Take-home message</h1>
  <h2>Invite yourself until you get invited</h2>
  <div class="fig-container" data-file="https://la.disneyresearch.com/publication/stereohaptics/" style="height: auto;" data-scrollable="yes"></div>
  <div class="cite"><a href="https://la.disneyresearch.com/publication/stereohaptics/">https://la.disneyresearch.com/publication/stereohaptics/</a></div>


<!--s-->
  <!-- .slide: id="section-haptic-floor" -->
  <h1>Haptic (tactile and kinesthetic) and audio</h1>
  <h2>Authoring for the Haptic Floor <br/>with proxies and by understanding explorable explanations</h2>
  <h3>Towards inclusive design with Blind people<h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html?t=haptic-floor" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->
  <h1>SAT's Haptic Floor</h1>
  <h2></h2>
  <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/290925507?h=af4b922189" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<p><a href="https://vimeo.com/290925507">Scalable Haptic Floor</a> from <a href="https://vimeo.com/satmetalab">Metalab | SAT</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
  <div class="cite"><a href="https://sat.qc.ca/en/haptic-floor">https://sat.qc.ca/en/haptic-floor</a> (~2018)</div>

<!--v-->

# Challenges

## How to author immersive experiences for the Haptic Floor?

<!--v-->

# Challenges

## How to author immersive experiences for the Haptic Floor...

### without experience in haptics?

### without access to the Haptic Floor?

### if authors and participants have specific accessibility needs?

<!--v-->

# Opportunities




<!--v-->

# Opportunities

## Students Showcase

* Current degree and major
* Project description / artifacts
* Methods/Methodologies learned
* Dissemination

<!--v-->

# Opportunities

## How to author immersive experiences for the Haptic Floor without experience in haptics?

### [Valentine Auphan](http://valentine.auphan.ensad.fr/)

<div class="authors">
<div class="author">
<img src="images/portraits/ValentineAuphan.jpeg" alt="Valentine Auphan"/>
</div>
</div>

<div class="row">
<div class="col-50">

* Master in Graphic Design at EnsAD (internship at the SAT)
* Haptic UI/UX design: [contextual inquiry](https://docs.google.com/forms/d/e/1FAIpQLSfwQhR35SD4Y76WYIKnSgwMQ9Pl6IvQ82jHocPAcGDaOztnXQ/viewform), [terminology study](https://docs.google.com/forms/d/e/1FAIpQLSeLPu5fXA-C85dnJYNYUwMjvcoezWHLZQhefu5Le4-t1Kexrg/viewform), low- to high-fidelity sketching

<div class="figure">
<img class="thumb" alt="HapticFloorUI croquis-2023-1-27_11-7-45" src="images/projects/HapticFloorUI/HapticFloorUX-Questionnaire1-ContextualSurvey-Header.png
"/>
</div>

</div>
<div class="col-50">

<div class="figure">
<!-- <img class="thumb" alt="HapticFloorUI croquis-2023-1-27_11-7-45" src="images/projects/HapticFloorUI/HapticFloorUX-Questionnaire1-ContextualSurvey-Header.png"/> -->

<img class="thumb" alt="HapticFloorUI croquis-2023-1-27_11-7-45" src="images/projects/HapticFloorUI/croquis-2023-1-27_11-7-45.png"/>
<div class="cite">
Valentine Auphan*, Claire Paillon, <b>Christian Frisson</b>.
Designing interactive haptic floor visualizations for monitoring displays and previewing immersive experiences.
To be submitted (ACM MM 2023 Demo | ACM SIGGRAPH 2023 Poster)
</div>
</div>

</div>
</div>


<!--v-->

# Opportunities

## How to author immersive experiences for the Haptic Floor without access to the Haptic Floor?

### [Raphaël Suzor-Schröder](https://www.linkedin.com/in/rapha%C3%ABl-suzor-schr%C3%B6der-6275431a9/)

<div class="authors">
<div class="author">
<img src="images/portraits/RaphaelSuzorSchroder.jpeg" alt="Raphaël Suzor-Schröder"/>
</div>
</div>

<div class="row">
<div class="col-50">

* Undergrad in Mech Eng at McGill (internships at the SAT)
* Haptic/Immersive UI design: CAD modelling, projection mapping


</div>
<div class="col-50">

<div class="figure">
<!-- https://www.youtube.com/watch?v=KY3mpczKI3k  -->
<video controls muted>
  <source alt="" src="images/projects/DeformableHapticSurfaces/DeformableHapticSurfaces-2022.mp4" type="video/mp4">
</video>
<!-- <img class="thumb" alt="" src=""/> -->
<div class="cite">
Raphaël Suzor-Schröder*, Eduardo A. L. Meneses, <b>Christian Frisson</b>.
An affordable open-source toolkit to discover interactive deformable surfaces / A proxy to the haptic floor from the SAT.
To be submitted (ACM MM 2023 Demo | ACM SIGGRAPH 2023 Poster).
</div>
</div>

</div>
</div>

<!--v-->

# Opportunities

## How to author immersive experiences for the Haptic Floor without access to the Haptic Floor?

### [Mathias Kirkegaard](http://www-new.idmil.org/people/mathias-kirkegaard/) and [Mathias Bredholt](http://www-new.idmil.org/people/mathias-bredholt/)

<div class="authors">
<div class="author">
<img src="images/portraits/MathiasKirkegaard.webp" alt="Mathias Kirkegaard"/>
<img src="images/portraits/MathiasBredholt.webp" alt="Mathias Bredholt"/>
</div>
</div>

<div class="row">
<div class="col-50">

* Master in Arts at McGill, IDMIL
* Haptic+Music UI development: physical computing, prototyping, mapping

</div>
<div class="col-50">

<div class="figure">
<!-- https://vimeo.com/404592134 -->
<video controls muted>
  <source alt="" src="images/projects/TorqueTuner/TorqueTuner + TStick [404592134].mp4" type="video/mp4">
</video>
<!-- <img class="thumb" alt="" src=""/> -->
<div class="cite">
Mathias Kirkegaard*, Mathias Bredholt*, <b>Christian Frisson</b>, Marcelo M. Wanderley. 
TorqueTuner: A self contained module for designing rotary haptic force feedback for digital musical instruments. 
In Proceedings of the 2020 International Conference on New Interfaces for Musical Expression (NIME 2020)
</div>
</div>

</div>
</div>

<!--v-->

# Opportunities

## How to author immersive experiences for the Haptic Floor without access to the Haptic Floor?

### [Albert-Ngabo Niyonsenga](http://www-new.idmil.org/people/albert-ngabo-niyonsenga/)

<div class="authors">
<div class="author">
<img src="images/portraits/AlbertNgaboNiyonsenga.webp" alt="Albert-Ngabo Niyonsenga"/>
</div>
</div>

<div class="row">
<div class="col-50">

* Master in Arts at McGill, IDMIL
* Haptic UI design: CAD modelling, firmware update, towards [Sustainable Haptic Development](http://www-new.idmil.org/project/sustainable-haptic-development/), video diaries

<div class="cite">
Albert-Ngabo Niyonsenga*, <b>Christian Frisson</b>, Marcelo Wanderley. 
TorqueTuner: a Case Study for Sustainable Haptic Development, 
11th International Workshop on Haptic & Audio Interaction Design, HAID 2022, 
Work-in-progress posters
</div>

</div>
<div class="col-50">

<div class="figure">
<!-- https://www.youtube.com/watch?v=KY3mpczKI3k -->
<video controls muted>
  <source alt="" src="images/projects/TorqueTuner/IDMIL Albert Christian TorqueTunerMoteus Demo Spring2022 [KY3mpczKI3k].mp4" type="video/mp4">
</video>
<!-- <img class="thumb" alt="SustainableHapticDevelopment" src="images/projects/TorqueTuner/SustainableHapticDevelopment.webp"/> -->
</div>

</div>
</div>



<!--v-->

# Opportunities

## How to author immersive experiences for the Haptic Floor if authors and participants have specific accessibility needs?
### Explorable explanations for learning and communication

<div class="row">
<div class="col-50">

>  Explorable explanations add interactivity to documents and figures

Explore and create explorable explanations to:

* Understand algorithmic concepts
* Create design documents for assignments and collaborations.

<!-- I am a contributor to the Idyll engine that will help students author explorable explanations. -->


</div>
<div class="col-50">

<div class="figure">
<img class="thumb" alt="WebAudioHapticsActivity" src="images/projects/WebAudioHapticsActivity.png"/>
<div class="cite">
<b>Christian Frisson</b>, Thomas Pietrzak, Siyan Zhao, Zachary Schwemler, Ali Israr. 
 <a href="https://webaudiohaptics.github.io/#/">WebAudioHaptics</a>: Tutorial on Haptics with Web Audio. 
 2nd Web Audio Conference 2016.
</div>
</div>

</div>
</div>


<!--v-->

# Opportunities

## How to author immersive experiences for the Haptic Floor if authors and participants have specific accessibility needs?
### Towards accessible explorable explanations 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ID2heZlnOJk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<div class="cite">
Marcelo M. Wanderley, Travis West, Josh Rohs, Eduado A. L. Meneses, Christian Frisson.
<a href="http://doi.org/10.1145/3478384.3478397">The IDMIL Digital Audio Workbench: An Interactive Online Application for Teaching Digital Audio Concepts.</a> 
In 16th AudioMostly Conference on Interaction with Sound. 2021. 
<a href="https://github.com/IDMIL/digitalaudioworkbench/commits/accessibility">https://github.com/IDMIL/digitalaudioworkbench/commits/accessibility</a>
</div>


<!--v-->

# Opportunities

## How to author immersive experiences for the Haptic Floor if authors and participants have specific accessibility needs?

  <div class="fig-container" data-file="https://image.a11y.mcgill.ca/" style="height: auto;" data-scrollable="yes"></div>
  <div class="cite"><a href="https://image.a11y.mcgill.ca/">https://image.a11y.mcgill.ca/</a> | <a href="https://listserv.acm.org/SCRIPTS/WA-ACMLPX.CGI?A2=CHI-JOBS;a2251693.2303D&S=">seeking Audio-Haptic Experience Designer</a> (2023-03-24)</div>

<!--v-->

# Opportunities
## Collaborate with local and global R&D communities

![](images/logos/GSoC-Horizontal.svg)

* **Haptic and audio interaction design** with Feelix supporting **TorqueTuner** and/or **DeformableHapticSurfaces**
* Interfacing MediaCycle for media library exploration in ossia score
* Motion Capture in WebXR 
* Blender add-on to create maps between UI signals and external controllers with **libmapper**
* **Accessibility** validation of user interfaces through continuous integration
* **Explorable explanations** for teaching digital arts concepts in hybrid telepresence campus
* Replace OpenGL by a multi-API rendering library in Splash

https://summerofcode.withgoogle.com/programs/2023/organizations/society-for-arts-and-technology-sat

https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/

Notes:
highlight on AR/MR/VR/XR and EDI, example: accessibility automation for authoring tools


<!--v-->

# Take-home message

## Intern in the industry (with the SAT)!

### [Google Summer of Code](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/)

### [MITACS](https://www.mitacs.ca) (Accelerate)

### [Regular internships](https://sat-mtl.gitlab.io/metalab/lab/categories/internships/)

<!--s-->
  <!-- .slide: id="section-satie" -->
  <h1>From audition to touch, <br/>towards olfaction and gustation</h1>
  <h2>Spatializing (ultra)sound haptics with SATIE</h2>
  <h3>Towards individualized immersions<h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html?t=SATIE" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->

<!--v-->

# Challenges

## How to design for other under-explored modalities?


<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/3rdparty/DC_logo_photo.jpg">
<div class="cite">Gustatory: <a href="http://data-cuisine.net">Data Cuisine</a></div>
</div>

<div class="figure">
<img class="thumb" alt="" src="images/3rdparty/Owidget.jpg">
<div class="cite">Olfactory: <a href="https://multi-sensory.info/index.php/component/spsimpleportfolio/item/11-owidgets">OWidgets</a></div>
</div>
</div>

Note: 
* What other senses can we explore for data display beyond vision, audition and touch? Note that the display of data through sound is a well-established field called sonification.
* How about the sense of taste? You see here the teaser of Data Cuisine by Moritz Stefaner and Susanne Jaschko.
* How about smell? OWidgets (Olfactory) by Mariana Obrist and her team at SCHI.

<!--v-->

# Opportunities

<!--v-->

# Opportunities

## Design for under-explored modalities with spatialized sound!

### [Ezra Pierce](https://www.linkedin.com/in/ezra-pierce-686958171/)

<div class="authors">
<div class="author">
<img src="images/portraits/EzraPierce.jpg" alt="Ezra Pierce"/>
</div>
</div>

<div class="row">
<div class="col-50">

* Bachelor of Engineering from **Carleton** (internship at the SAT)
* Opensource hardware replication for ultrasound haptics: sourcing parts, soldering, testing.


</div>
<div class="col-50">

Towards mulsemedia design with: 

* highly directional audio displays
* ultrasound haptics displays
* gustatory / olfactory display

<div class="figure">
<div class="cite">
Ezra Pierce*, Eduardo A. L. Meneses, Michał Seta, <b>Christian Frisson</b>.
Spatialized ultrasound haptics. 
To be submitted (ACM MM 2023 Demo + IEEE ToH Short)
</div>
</div>

</div>
</div>


<!--v-->

# Opportunities

## Measure how people experience modalities with accessibility and computer vision

<iframe width="560" height="315" src="https://www.youtube.com/embed/xv-g04fn9pE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<div class="cite">
<b>Christian Frisson</b>, Sylvain Malacria, Gilles Bailly, and Thierry Dutoit. 
<a href="https://doi.org/10.1145/2851581.2892388">InspectorWidget: A System to Analyze Users Behaviors in Their Applications.</a>
In Proceedings of the 2016 CHI Conference Extended Abstracts on Human Factors in Computing Systems (CHI EA '16). ACM.  
</div>

<!--v-->

# Take-home message

## Find and keep mentor(s)

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Thread with advice on applying to <a href="https://twitter.com/hashtag/grad?src=hash&amp;ref_src=twsrc%5Etfw">#grad</a> school:<br><br>Finding a good supervisor is more important than being in the better university on paper/ranking. You want to be in a good environment to do research for a few years. You should get along with your supervisor. (1/9)</p>&mdash; Dr. Audrey Girouard @audreygirouard@hci.social (@Audreygirouard) <a href="https://twitter.com/Audreygirouard/status/1181905760713879552?ref_src=twsrc%5Etfw">October 9, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


<!--s-->
  <!-- .slide: id="sat-tools-redux-outro" -->
  <h1>Ecosystem of tools at the SAT</h1>
  <h2>Inter-operability and modalities</h2>
  <h3>Redux</h3>
  <div class="fig-container" data-file="images/sat-tools/redux.html" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->


<!--v-->
  <!-- .slide: id="sat-tools-outro" -->
  <h1>Ecosystem of tools at the SAT</h1>
  <h2>Inter-operability and modalities</h2>
  <div class="fig-container" data-file="images/sat-tools/default.html" style="overflow: visible;"></div>
  <!-- <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div> -->


<!--s-->

<!-- .slide: id="end" -->
  <h2 data-i18n="title">Challenges and Opportunities <br/>for Equity, Diversity, Inclusion, and Accessibility <br/>in Immersive Arts</h2>
  <h3 data-i18n="conference"><a href="https://www.csit.carleton.ca/">CSIT</a> ITEC 5001 <br/>Graduate Student Seminar</h3>
  <h4 data-i18n="subtitle">2023-04-10</h4>

  <div class="authors">
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name">
      <a href="mailto:christian@frisson.re" title="Christian Frisson's Email">Christian</a></span> <a href="https://frisson.re" title="Christian Frisson's Website">Frisson</a></span>
      </div>
    </div>
  </div>


<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/1-LOGO_SAT_noir_sans_texte.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

[[SAT](https://sat.qc.ca/)] [metalab](https://sat-mtl.gitlab.io/metalab/)
