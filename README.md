# Carleton School of Information Technology (CSIT) Graduate Seminar Invited Talk

Monday, Apr 10th 2023, 12:00 pm to 1:00 pm

Azrieli Pavilion (AP) 238 

1125 Colonel By Dr, Ottawa, ON K1S 5B6, Canada

In Person – Pizza will be provided for this event

## Speaker

Christian Frisson ([https://frisson.re](https://frisson.re/))
- Researcher and Developer, Metalab, Society for Arts and Technology (SAT)
- Contract Instructor (ITEC 5207 F22), CSIT, Carleton University
- Associate Researcher, IDMIL, McGill University

## Title

Challenges and Opportunities for Equity Diversity, Inclusion and Accessibility in Immersive Arts

## Abstract

In this invited talk, I will present challenges and opportunities for equity, diversity, inclusion and accessibility (EDIA) in immersive arts, towards a submission to Frontiers in VR, special issue on Inclusion in VR, whose special editors include Ali Arya and Rob Teather from CSIT. I will walk through 4 use cases on the different **human senses** that can be stimulated in immersive arts, the **related modalities** that we create *tools* to author with at the SAT, <u>perspectives for EDIA</u>, and take-home messages for graduate students to support their professional strategies:  
- **computer vision**: camera-based group interaction with *LivePose*, <u>towards training pose and action for diversity, inclusion and accessibility</u>  
- **human vision**: projection mapping and colorimetry calibration with *Splash*, <u>towards compensating for vision impairments</u>  
- **haptic (tactile and kinesthetic) and audio**: authoring for the *Haptic Floor* with proxies and by understanding haptic explorable explanations, <u>towards inclusive design with Blind people</u>  
- **from audition to touch, towards olfaction and gustation**: spatializing (ultra)sound haptics with *SATIE*, <u>towards individualized immersions</u>

## Speaker Biography 

[Christian Frisson](https://frisson.re/) is a Researcher and Developer at the Society for Arts and Technology ([SAT](https://sat.qc.ca/)), [Metalab](https://sat.qc.ca/en/metalab); Contract Instructor at Carleton University, School of Information Technology ([CSIT](https://www.csit.carleton.ca/)), ITEC 5207 F22; and Associate Researcher with the Input Devices and Music Interaction Lab ([IDMIL](https://www.idmil.org/people/christian-frisson/)). Christian was previously Postdoctoral Researcher: at McGill University, [IDMIL](https://www.idmil.org/people/christian-frisson/), funded by his Mitacs Elevate Fellowship with [Haply](https://www.haply.co/), including on creating open-source [Haptic Devices](https://ised-isde.canada.ca/site/innovative-solutions-canada/en/haptic-system) with National Research Council Canada ([NRC](https://nrc.canada.ca/)); at the University of Calgary, [Interactions Lab](http://ilab.cpsc.ucalgary.ca/), on [EnergyVis](https://ilab.cpsc.ucalgary.ca/energyvis/), funded by Canada Energy Regulator ([CER](https://www.cer-rec.gc.ca/)), for whom they obtained [the IEEE VIS 2019 InfoVis Best Paper Award](https://ieeevis.org/year/2019/info/awards/best-paper-awards); and at Inria, [Mjolnir](http://mjolnir.lille.inria.fr/)/[Loki](https://loki.lille.inria.fr/) team, funded by EU project [HAPPINESS](https://cordis.europa.eu/project/id/645145) with partners including The French Alternative Energies and Atomic Energy Commission ([CEA](https://www.cea.fr/)). Christian obtained his PhD at the University of Mons, [numediart](http://numediart.org/) Institute, including collaborations with [Dame Blanche](http://www.dameblanche.com/) studios and [Disney Research](https://la.disneyresearch.com/publication/stereohaptics/).

## Presentation

Source: [presentation.md](presentation.md)

### Requirements

- Install Node.js and NPM.
- Install dependencies (once or after upgrades):
```
npm i
```

### Development

Write markdown while [reveal-md](https://github.com/webpro/reveal-md)'s local server converts to html slides.

- Run the local server:
```
npm run dev
```

### Release

- Convert md to html:
```
npm run html
```

### Controls

- Use `Ctrl + click` to zoom on slides, useful for images
- Press `o` to show the slide overview
- Press `s` to open the speaker notes window (requires allowing popups)

Check the [reveal.js](https://github.com/hakimel/reveal.js/#speaker-notes) README for more tips.
